import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
emailValue="";
passwordValue="";
repasswordValue="";
isShow=true;
viewType: String= "login";

  constructor(private authService:AuthServiceService, private router:Router) { }

  ngOnInit(): void {
  }

  onLogin(): void{
    console.log(this.emailValue);
    console.log(this.passwordValue);
    // alert("Email: "+this.emailValue+", password: "+this.passwordValue);
    this.authService.login(this.emailValue,this.passwordValue).subscribe((response)=>{
      console.log(response);
      this.router.navigate(["/dashboard"]);
    })
  }

  onRegister(): void{
    console.log(this.emailValue);
    console.log(this.passwordValue);
    console.log(this.repasswordValue);
    // alert("Email: "+this.emailValue+", password: "+this.passwordValue+", repassword: "+this.repasswordValue);
    if(this.passwordValue==this.repasswordValue){
        console.log("Password match.");
      this.authService.register(this.emailValue,this.passwordValue).subscribe((response)=> {
        console.log(response);
      })

      }else {
        console.log("Invalid password!");
      }
    }

  onShow():void{
    // console.log("before: "+this.isShow);
    // this.isShow=!this.isShow;
    // console.log("after: "+this.isShow);

    if(this.viewType=='login'){
      this.viewType="register";
    }else{
      this.viewType="login";
    }
  }
}
