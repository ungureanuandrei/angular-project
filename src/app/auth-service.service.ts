import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http:HttpClient) { }

  login(email:String, password:String){
    return this.http.post(`${environment.baseUrl}/api/auth/login`, {email:email,password:password});
  }

  register(email: String, password: String){
    return this.http.post(`${environment.baseUrl}/api/auth/register`, {email: email, password: password});
  }

}
